package java_process;

import java.util.ArrayList;
import java.util.List;

import core.Abstract_Process;
import core.Count_Type;
import run.LaunchPIP;

public class Java_Process extends Abstract_Process {
	private Count_Type packages;
	private Count_Type classes;
	private Count_Type linesOfCode;
	private Count_Type linesOfCodeWithEmptyLines;
	private List<String> specialPackageCount;

	public Java_Process() {
		packages = new Count_Type("packages", "counts all java packages in src folder", 0);
		classes = new Count_Type("classes", "counts all java classes in src folder", 0);
		linesOfCode = new Count_Type("lines of code",
				"counts real lines of code in java files (no empty lines counted)", 0);
		linesOfCodeWithEmptyLines = new Count_Type("loc with empty lines", "counts all lines of code in java files", 0);
		specialPackageCount = new ArrayList<>();
	}

	@Override
	public String getLanguageName() {
		return "Java";
	}

	@Override
	public String getLanguageDescription() {
		return "counting process for java language files, folders and lines of code";
	}

	@Override
	public boolean inspectFolder(String folder) {
		if (processFolder(folder)) {
			packages.incrementCounter();
			if (LaunchPIP.debug_mode)
				System.out.println("+1 package - count is " + packages.getCount());
		}
		return true; //inspect all folders
	}

	@Override
	public boolean inspectFile(String file) {
		if (!processFolder(file)) { //don't inspect file if not a java file in a src folder (getting the whole path)
			return false;
		}
		if (file.contains(".java")) {
			classes.incrementCounter();
			if (LaunchPIP.debug_mode)
				System.out.println("+1 class - count is " + classes.getCount());
			return true;
		}
		return false;
	}

	private boolean processFolder(String folder) {
		boolean count = folder.contains("src") || folder.contains("ejbModule") || folder.contains("test");

		boolean contains = false;
		for (String s : specialPackageCount) {
			if (s.contains("src") || s.contains("ejbModule") || s.contains("test")) {
				contains = true;
			}
		}
		if (!contains && count) {
			specialPackageCount.add(folder);
		}
		return count;
	}

	@Override
	public void checkInputLine(String line) {
		linesOfCodeWithEmptyLines.incrementCounter();
		if (line == null || line.trim().equals("")) {
			return;
		}
		linesOfCode.incrementCounter();
	}

	@Override
	public List<Count_Type> getCounters() {
		List<Count_Type> res = new ArrayList<>();

		packages.setCount(packages.getCount() - specialPackageCount.size());

		res.add(packages);
		res.add(classes);
		res.add(linesOfCode);
		res.add(linesOfCodeWithEmptyLines);
		return res;
	}

}
