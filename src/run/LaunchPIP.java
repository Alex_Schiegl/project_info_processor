package run;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import core.Abstract_Process;
import core.Count_Type;
import core.Processor;
import css_process.CSS_Process;
import html_process.HTML_Process;
import java_process.Java_Process;

public class LaunchPIP {
	public static File base_dir;
	public static boolean debug_mode;
	private static List<Abstract_Process> processes = new LinkedList<>();

	public static void main(String[] args) {
		base_dir = new File(System.getProperty("user.dir"));

		parseParams(args);

		startPIP();

		printStatistics();
	}

	private static void parseParams(String[] args) {
		if (args.length == 0) {
			System.err.println("At least one parameter must be set.");

			System.out.println("Usage of parameters:");
			System.out.println("Set path to inspect:");
			System.out.println("path=$mypath");
			System.out.println();
			System.out.println("If the path is not set, the current directory is chosen");
			System.out.println();
			System.out.println("Enable Java inspection:");
			System.out.println("java");
			System.out.println();
			System.out.println("Enable HTML inspection:");
			System.out.println("html");
			System.out.println();
			System.out.println("Enable CSS inspection:");
			System.out.println("css");
			System.out.println();
			System.out.println("Each param seperated per white space \" \"");

			System.exit(0);
		}

		for (String s : args) {
			if (s.startsWith("path=")) {
				try {
					File cur = new File(s.substring(5));
					if (!cur.exists() || !cur.canRead()) {
						System.err.println("Path does not exist.");
						continue;
					}
					base_dir = cur;
				}
				catch (Exception e) {
					System.err.println("Path not available.");
					e.printStackTrace();
				}
			}
			else if (s.equals("debug")) {
				debug_mode = true;
			}
			else if (s.equals("java")) {
				processes.add(new Java_Process());
			}
			else if (s.equals("css")) {
				processes.add(new CSS_Process());
			}
			else if (s.equals("html")) {
				processes.add(new HTML_Process());
			}
		}
		System.out.println("Generating statistics for following path/project:");
		System.out.println(base_dir.getAbsolutePath());
	}

	private static void startPIP() {
		for (Abstract_Process ap : processes) {
			Processor p = new Processor(ap);
			p.startProcess();
		}
	}

	private static void printStatistics() {
		System.out.println();

		for (Abstract_Process ap : processes) {
			System.out.println(ap.getLanguageName() + " -- " + ap.getLanguageDescription());
			for (Count_Type ct : ap.getCounters()) {
				System.out.printf("%-5s %-23s %s\n", ct.getCount(), ct.getType(), ct.getDescription());
			}
			System.out.println();
		}
	}
}
