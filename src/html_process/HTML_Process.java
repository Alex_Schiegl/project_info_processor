package html_process;

import java.util.ArrayList;
import java.util.List;

import core.Abstract_Process;
import core.Count_Type;

public class HTML_Process extends Abstract_Process {
	private Count_Type files;
	private Count_Type linesOfCode;
	private Count_Type linesOfCodeWithEmptyLines;

	public HTML_Process() {
		files = new Count_Type("HTML files", "counts all .html files", 0);
		linesOfCode = new Count_Type("lines of code ", "counts lines of real code", 0);
		linesOfCodeWithEmptyLines = new Count_Type("loc with empty lines", "counts all lines of code with empty lines",
				0);
	}

	@Override
	public String getLanguageName() {
		return "HTML";
	}

	@Override
	public String getLanguageDescription() {
		return "counts HTML and XHTML files and lines of code";
	}

	@Override
	public boolean inspectFolder(String folder) {
		return true;
	}

	@Override
	public boolean inspectFile(String file) {
		if (file.contains(".html") || file.contains(".xhtml")) {
			files.incrementCounter();
			return true;
		}
		return false;
	}

	@Override
	public void checkInputLine(String line) {
		linesOfCodeWithEmptyLines.incrementCounter();

		if (line == null || line.trim().equals("")) {
			return;
		}
		linesOfCode.incrementCounter();
	}

	@Override
	public List<Count_Type> getCounters() {
		List<Count_Type> counters = new ArrayList<>();
		counters.add(files);
		counters.add(linesOfCode);
		counters.add(linesOfCodeWithEmptyLines);
		return counters;
	}

}
