package core;

/**
 * Count_Type is a class for counting normal and also custom elements. Normal
 * elements are files, folders, lines. Custom elements can be e.g. CSS classes
 * or so on.
 */
public class Count_Type {
	/**
	 * The count-types are used to determine similar fields for the summary at
	 * the end of the execution. So for e.g. all counttype_folder are summarized
	 * and displayed and so on.
	 */
	public static final String COUNTTYPE_FOLDER = "counttype_folder";

	/**
	 * The count-types are used to determine similar fields for the summary at
	 * the end of the execution. So for e.g. all counttype_file are summarized
	 * and displayed and so on.
	 */
	public static final String COUNTTYPE_FILE = "counttype_file";

	/**
	 * The count-types are used to determine similar fields for the summary at
	 * the end of the execution. So for e.g. all counttype_line are summarized
	 * and displayed and so on.
	 */
	public static final String COUNTTYPE_LINE = "counttype_line";

	/**
	 * The short description of what will be counted. Like: package, class, ...
	 */
	private String type;

	/**
	 * A longer description of what will be counted. This is optional.
	 */
	private String description;

	/**
	 * The corresponding count.
	 */
	private int count;

	public Count_Type(String type, String description, int count) {
		this.type = type;
		this.description = description;
		this.count = count;
	}

	public void incrementCounter() {
		count++;
	}

	public void decreaseCounter() {
		count--;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
