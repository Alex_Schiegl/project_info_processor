package core;

import java.util.List;

public abstract class Abstract_Process {
	public abstract String getLanguageName();

	public abstract String getLanguageDescription();

	/**
	 * @return true if the folder should be inspected. In java a folder should
	 *         not be inspected if it is a bin folder for example. Self count
	 *         for incrementing folder count. Always returns the complete path
	 *         from the original selected root directory (start directory).
	 */
	public abstract boolean inspectFolder(String folder);

	/**
	 * @return true if the file should be inspected. A java file should be
	 *         inspected if the extension is .java for example. Self count for
	 *         incrementing file count.
	 */
	public abstract boolean inspectFile(String file);

	/**
	 * Invoked, when a new line appears in a inspected file. Self count for
	 * incrementing line count for example.
	 */
	public abstract void checkInputLine(String line);

	/**
	 * 
	 * @return A List of all counted elements of the Type Count_Type. Which are
	 *         displayed properly.
	 */
	public abstract List<Count_Type> getCounters();
}
