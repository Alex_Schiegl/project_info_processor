package core;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import run.LaunchPIP;

public class Processor {
	private Abstract_Process process;

	public Processor(Abstract_Process process) {
		this.process = process;
	}

	public void startProcess() {
		inspectFolder(LaunchPIP.base_dir);
	}

	private void inspectFolder(File folder) {
		File[] elements = folder.listFiles();
		if (LaunchPIP.debug_mode)
			System.out.println("Folder visited: " + getRelativePath(folder));
		for (File f : elements) {
			if (f.getAbsolutePath().contains("project_info_processor")) {
				continue;
			}
			if (f.isFile()) {
				if (process.inspectFile(getRelativePath(f))) {
					inspectFile(f);
				}
			}
			else if (f.isDirectory()) {
				if (process.inspectFolder(getRelativePath(f))) {
					inspectFolder(f);
				}
			}
		}
	}

	private void inspectFile(File file) {
		if (LaunchPIP.debug_mode)
			System.out.println("File visited: " + getRelativePath(file));

		Scanner c = null;
		try {
			c = new Scanner(file);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		while (c.hasNextLine()) {
			String line = c.nextLine();
			process.checkInputLine(line);
		}
	}

	private String getRelativePath(File f) {
		return f.getAbsolutePath().replace(LaunchPIP.base_dir.getAbsolutePath(), "");
	}
}
